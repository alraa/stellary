$(document).ready(function() {

    //E-mail Ajax Send
    //Documentation & Example: https://github.com/agragregra/uniMail
    $(".send-form").submit(function() { //Change
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "mail.php", //Change
            data: th.serialize()
        }).done(function() {
            $.fancybox({
                    href: '#thanks-popup',
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                    afterLoad: function() {
                      setTimeout(function() {
                        parent.$.fancybox.close({
                          closeEffect: 'elastic'
                        });
                      }, 3000);
                    }
                  });
                    });
            setTimeout(function() {
                // Done Functions
                th.trigger("reset");
            }, 1000);
        return false;
        });

	$('#video')[0].muted ^= 1;
	$('#volume').click(function(){
	   $('video')[0].muted ^= 1;
	   $(this).toggleClass('mute unmute');
	});

	//placeholder
	$('[placeholder]').focus(function() {
		var input = $(this);
		if(input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if(input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if(input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

	//
	$('.video-link').on('click', function(ev) {
    	$("#homevideo")[0].src += "&autoplay=1";
    	$(this).parent().addClass('play');
    	ev.preventDefault();
	});


	// map js
    var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map-container'), {
      zoom: 11,
      center: new google.maps.LatLng(-33.92, 151.25),
      scrollwheel: false,
      styles: [
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            }
          ],
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    var image = new google.maps.MarkerImage('img/map_active.png',
      new google.maps.Size(71, 42),
      new google.maps.Point(0,0),
      new google.maps.Point(0, 42));

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        icon: image,
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    //
    $('.looks').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable:false,
        infinite: true,
        arrows: true,
        fade: true,
        dots: true,
        customPaging : function(slider, i) {
	        var title = $(slider.$slides[i]).data('title');
	        return '<a class="pager__item"> '+title+' </a>';
	    },
	    nextArrow: '<div type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><b><i></i></b><span class="circle"></span></div>',
        speed: 800,
        asNavFor: '.bg-slider'
    });
    $('.bg-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable:false,
        infinite: true,
        arrows: false,
        fade: true,
        dots: false,
        speed: 500,
        asNavFor: '.looks',
		focusOnSelect: true
    });

    $('.looks').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
	    $(slick.$slides[currentSlide]).addClass('out');
	    $(slick.$slides[nextSlide]).addClass('animate');
	});
	$('.looks').on('afterChange', function(event, slick, currentSlide, nextSlide) {
	    $(slick.$slides).removeClass('animate').removeClass('out');
	});

	//fancybox
	$(window).load(function() {
	$('.fancy').fancybox({
		'afterLoad'          : function () { $(".main-wrapper").addClass("open"); },
        'afterClose'         : function () { $(".main-wrapper").removeClass("open"); },
        openEffect  : 'fade', // 'elastic', 'fade' or 'none'
		openSpeed   : 200,
		openEasing  : 'swing',
		openOpacity : true,
		openMethod  : 'zoomIn',
	});
	});	

	//js-box-opener
	$('.js-box-opener').on('click', function(ev) {
    	$('.box').addClass('open');
    	$(this).hide(0);
    	$('.js-btn-video').addClass('visible');
    	return false;
	});
	$('.js-btn-video').on('click', function(ev) {
    	$("html, body").animate({ scrollTop: $('.section-video').offset().top }, "700");
		return false;
	});

    // GLAM
	
    $('.js-tovs-slider-1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable:false,
        infinite: true,
        arrows: true,
        fade: true,
        dots: false,
        speed: 800,
        asNavFor: '.js-look-items-1'
    });
    $('.js-look-items-1').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        draggable:false,
        infinite: false,
        variableWidth:true,
        arrows: false,
        fade: false,
        dots: false,
        speed: 500,
        asNavFor: '.tovs-slider-1',
        focusOnSelect: true
    });

        //
        $('.js-glam a.look-item[data-slide]').click(function(e) {
           e.preventDefault();
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-1').slick('slickGoTo', slideno - 1);
        });
		
		$('.js-look-items-1 .slick-slide a').click(function(e) {
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-1').slick('slickGoTo', slideno - 1);
        });
		

    // WILD
    $('.js-tovs-slider-2').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable:false,
        infinite: true,
        arrows: true,
        fade: true,
        dots: false,
        speed: 800,
        asNavFor: '.js-look-items-2'
    });
    $('.js-look-items-2').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        draggable:false,
        infinite: false,
        variableWidth:true,
        arrows: false,
        fade: false,
        dots: false,
        speed: 500,
        asNavFor: '.tovs-slider-2',
        focusOnSelect: true
    });

        //
        $('.js-wild a.look-item[data-slide]').click(function(e) {
           e.preventDefault();
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-2').slick('slickGoTo', slideno - 1);
        });
		$('.js-look-items-2 .slick-slide a').click(function(e) {
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-2').slick('slickGoTo', slideno - 1);
        });
		

    // RICH
    $('.js-tovs-slider-3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable:false,
        infinite: true,
        arrows: true,
        fade: true,
        dots: false,
        speed: 800,
        asNavFor: '.js-look-items-3'
    });
    $('.js-look-items-3').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        draggable:false,
        infinite: false,
        variableWidth:true,
        arrows: false,
        fade: false,
        dots: false,
        speed: 500,
        asNavFor: '.tovs-slider-3',
        focusOnSelect: true
    });

        //
        $('.js-rich a.look-item[data-slide]').click(function(e) {
           e.preventDefault();
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-3').slick('slickGoTo', slideno - 1);
        });
		
		$('.js-look-items-3 .slick-slide a').click(function(e) {
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-3').slick('slickGoTo', slideno - 1);
        });

	// ULTRA
    $('.js-tovs-slider-4').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable:false,
        infinite: true,
        arrows: true,
        fade: true,
        dots: false,
        speed: 800,
        asNavFor: '.js-look-items-4'
    });
    $('.js-look-items-4').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        draggable:false,
        infinite: false,
        variableWidth:true,
        arrows: false,
        fade: false,
        dots: false,
        speed: 500,
        asNavFor: '.tovs-slider-4',
		focusOnSelect: true
    });

        //
        $('.js-ultra a.look-item[data-slide]').click(function(e) {
    	   e.preventDefault();
    	   var slideno = $(this).data('slide');
    	   $('.js-tovs-slider-4').slick('slickGoTo', slideno - 1);
    	});
		$('.js-look-items-4 .slick-slide a').click(function(e) {
           var slideno = $(this).data('slide');
           $('.js-tovs-slider-4').slick('slickGoTo', slideno - 1);
        });

	$('a.look-link[data-slide]').click(function(e) {
	   e.preventDefault();
	   var slideno = $(this).data('slide');
	   $('.looks').slick('slickGoTo', slideno - 1);
	   // $("html, body").animate({ scrollTop: $('.section-looks').offset().top }, "700");
		return false;
	});

	//маска для поля ввода телефона
	$('input[type="tel"]').mask('+7 (999) 999-99-99',{placeholder:"+7 (___) ___-__-__"});

	//
	var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('.spinner');
            $spinner.fadeOut();
        $preloader.delay(200).fadeOut('slow');

    // tooltips

    $('.js-more-info').click(function () {
        $('.lay__tooltip').css('display', 'none');
        $('.lay').css('z-index', '15');
        $(this).closest('.lay').css('z-index', '100');
        $(this).closest('.lay').find('.lay__tooltip').css('display', 'block');
    })

    $('.js-tooltip-close').click(function () {
        $('.lay__tooltip').css('display', 'none');
        $(this).closest('.lay').css('z-index', '15');
        // $(this).closest('.lay').find('.lay__tooltip').css('display', 'block');
    })

});

if($(window).width() > 1024){
	skrollr.init({
		forceHeight: false,
		smoothScrolling:true,
        smoothScrollingDuration:1e3,
	});
}

if($(window).width() < 1025){
    $(document).scroll(function() {
      if ($(document).scrollTop() >= 20) {
        $('#header').addClass('scrolled');
      } else {
        $('#header').removeClass('scrolled');
      }
    });
}



/*
$.fn.moveIt = function(){
  var $window = $(window);
  var instances = [];
  
  $(this).each(function(){
    instances.push(new moveItItem($(this)));
  });
  
  window.onscroll = function(){
    var scrollTop = $window.scrollTop();
    instances.forEach(function(inst){
      inst.update(scrollTop);
    });
  }
}

var moveItItem = function(el){
  this.el = $(el);
  this.speed = parseInt(this.el.attr('data-scroll-speed'));
};

moveItItem.prototype.update = function(scrollTop){
  var pos = scrollTop / this.speed;
  this.el.css('transform', 'translateY(' + -pos + 'px)');
};

$(function(){
	$('[data-scroll-speed]').moveIt();
});
*/